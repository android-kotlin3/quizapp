# QuizApp

The following application is to perform a quiz about countries, the objective is to learn how to manage application states,
and manage form steps

## Project Summary

For this project, the idea of doing a quiz was worked, validating the correct and incorrect answers when interacting
with the selectable answers, the control of data structures and constants was applied at an intermediate level to handle
validations of total answers, it was also I interact with input validations to ask the user for the name.

Another important aspect is that Activities control is handled and pass fields or attributes to other activities through Intents

## Project Download

```
git clone https://gitlab.com/android-kotlin3/quizapp.git
```

## development libraries

For this project we interact a little more and Material Design components as described later in the version:

```
implementation 'com.google.android.material:material:1.0.0'
```
***

## Solution to present:

***

![](/markdown/video.gif "DEMO PROJECT")
